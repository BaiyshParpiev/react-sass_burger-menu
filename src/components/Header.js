import React, {useEffect, useState} from 'react';
import {BiMenuAltRight} from 'react-icons/bi'
import {AiOutlineClose} from 'react-icons/ai'
import classes from './Header.module.scss';
import {Link, useNavigate} from "react-router-dom";

const Header = () => {
    const navigate = useNavigate();
    const [menuOpen, setMenuOpen] = useState(false);
    const [size, setSize] = useState({width: undefined, height: undefined});
    const menuToggleHandler = () => {
        setMenuOpen(!menuOpen);
    }

    useEffect(() => {
        const handleResize = () =>{
            setSize({
                width: window.innerWidth,
                height: window.innerHeight
            });
        };

        window.addEventListener('resize', handleResize);

        return () => window.removeEventListener('resize', handleResize)
    }, []);


    useEffect(() => {
        if(size.width > 760 && menuOpen){
            setMenuOpen(false)
        }
    }, []);

    const ctacClickHandler = () => {
        menuToggleHandler();
        navigate('/pageCTA')
    }

    return (
        <header className={classes.header}>
            <div className={classes.header__content}>
                <Link to="/" className={classes.header__content__logo}>navbar</Link>
                <nav className={`${classes.header__content__nav} ${menuOpen && size.width < 768 ? classes.isMenu : ""}`}>
                    <ul>
                        <li><Link to="/pageOne" onClick={menuToggleHandler}>Page One</Link></li>
                        <li><Link to="/pageTwo" onClick={menuToggleHandler}>Page Two</Link></li>
                        <li><Link to="pageThree/" onClick={menuToggleHandler}>Page Three</Link></li>
                    </ul>
                    <button onClick={ctacClickHandler}>CTA Page</button>
                </nav>
                <div className={classes.header__content__toggle}>
                    {!menuOpen ? <BiMenuAltRight onClick={menuToggleHandler}/> : <AiOutlineClose onClick={menuToggleHandler}/>}
                </div>
            </div>
        </header>
    );
};

export default Header;